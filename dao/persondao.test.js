var mysql = require("mysql");

const PersonDao = require("./persondao.js");
const runsqlfile = require("./runsqlfile.js");

// GitLab CI Pool
var pool = mysql.createPool({
  connectionLimit: 1,
  host: "mysql.stud.iie.ntnu.no",
  user: "janmariv",
  password: "cdgRaKKE",
  database: "janmariv",
  debug: false,
  multipleStatements: true
});

let personDao = new PersonDao(pool);

beforeAll(done => {
  runsqlfile("dao/create_tables.sql", pool, () => {
    runsqlfile("dao/create_testdata.sql", pool, done);
  });
});

test("get one person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(1);
    expect(data[0].navn).toBe("Hei Ssveisen");
    done();
  }

  personDao.getOne(1, callback);
});

test("get unknown person from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.length).toBe(0);
    done();
  }

  personDao.getOne(0, callback);
});

test("add person to db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data=" + JSON.stringify(data)
    );
    expect(data.affectedRows).toBeGreaterThanOrEqual(1);
    done();
  }

  personDao.createOne(
    { navn: "Nils Nilsen", alder: 34, adresse: "Gata 3" },
    callback
  );
});

test("get all persons from db", done => {
  function callback(status, data) {
    console.log(
      "Test callback: status=" + status + ", data.length=" + data.length
    );
    expect(data.length).toBeGreaterThanOrEqual(2);
    done();
  }

  personDao.getAll(callback);
});

// test("update person", done => {
//   function callback(status, data) {
//     console.log(
//         "Test callback: status=" + status + ", data=" + JSON.stringify(data)
//       );
//     expect(data.affectedRows).toBeGreaterThanOrEqual(1);
//     done();
//   }
//   personDao.updateOne(2,
//       {navn: "Ådne", alder: "455", adresse: "Gate40"},
//       callback);
// });


test("update person", done => {
    const id = 3;
    const navn = 'Ådne';
    const alder = 20;
    const adresse = 'Solsiden';

    personDao.updateOne(id,{navn, alder, adresse}, (status, data) => {
        expect(data.affectedRows).toBe(1);
        personDao.getOne(id, (status, data) => {
            expect(data[0].navn === navn).toBeTruthy();
            expect(data[0].alder === alder).toBeTruthy();
            expect(data[0].adresse === adresse).toBeTruthy();
            done();
        });
        done();
    });
});


test("delete person from db", done => {
  personDao.getAll((status, data) => {
    personDao.deleteOne(, (status, data2) => {
      expect(data2.affectedRows).toBe(1);
      personDao.getAll((status, data3) => {
        expect(data3.length + 1 === data.length).toBe(true);
      });
    });
    done();
  });
});





